# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
import copy

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.

    Your search algorithm needs to return a list of actions that reaches the
    goal. Make sure to implement a graph search algorithm.

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:

    print "Start:", problem.getStartState()
    print "Is the start a goal?", problem.isGoalState(problem.getStartState())
    print "Start's successors:", problem.getSuccessors(problem.getStartState())
    """
    "*** YOUR CODE HERE ***"
    visitedNodes = util.Counter()
    actions = util.Stack()
    stack = util.Stack()
    s = problem.getStartState()
    if(problem.isGoalState(s)):
        return []
    stack.push(problem.getSuccessors(s))
    while(not(stack.isEmpty())):
        if(len(stack.list[-1])==0):
            stack.pop()
            actions.pop()
            continue
        else:
            vertex = stack.list[-1].pop()
            [position, direction, cost] = vertex
            visitedNodes[position]+=1
            actions.push(direction)
            if(problem.isGoalState(position)):
                return actions.list
            else:
                neighbours = problem.getSuccessors(position)
                unexpandedNodes = []
                for n in neighbours:
                    if(visitedNodes[n[0]]==0):
                        unexpandedNodes.append(n)
                if(len(unexpandedNodes)>0):
                    stack.push(unexpandedNodes)
                else:
                    actions.pop()
    return actions.list

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    visitCount = util.Counter()
    front = []
    s = problem.getStartState()
    visitCount[s]=1
    if(problem.isGoalState(s)):
        return []
    front = problem.getSuccessors(s)
    for i in range(0, len(front)):
        v = front[i]
        front[i] = (v[0], [v[1]], v[2])
    while(len(front)>0):
        newFront = []
        for v in front:
            [position, directions, cost] = v
            if(problem.isGoalState(position)):
                return directions
            if(visitCount[position]>0):
                continue
            visitCount[position]+=1
            adj=problem.getSuccessors(position)
            for j in range(0, len(adj)):
                p = copy.copy(v[1])
                p.append(adj[j][1])
                adj[j] = (adj[j][0], p, adj[j][2])
            newFront.extend(adj)
        front = newFront
    return []

class MyTriplet:
    def __init__(self, state, directions, cost):
        self.state = state
        self.directions = directions
        self.cost = cost

    def __eq__(self, other):
        return self.state == other.state

    def getState(self):
        return self.state

    def getDirections(self):
        return self.directions

    def getCost(self):
        return self.cost

    def __str__(self):
        return "{0}\t{1}\t{2}".format(self.state, self.cost, self.directions)

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    # Reference
    # http://www.ccs.neu.edu/home/rplatt/cs5335_2016/slides/bfs_ucs.pdf - slide 62
    visitCount = util.Counter()
    pq = util.PriorityQueue()
    start_state = problem.getStartState()
    visitCount[start_state] = 1
    if(problem.isGoalState(start_state)):
        return []
    start_state_successors = problem.getSuccessors(start_state)
    print start_state_successors
    for i in range(0, len(start_state_successors)):
        [state, direction, cost] = start_state_successors[i]
        pq.push( MyTriplet(state, [direction], cost) ,  cost)
    while(not(pq.isEmpty())):
        v = pq.pop();
        # print v
        v_position = v.getState()
        v_directions = v.getDirections()
        v_cost = v.getCost()
        if(problem.isGoalState(v_position)):
            return v_directions
        if(visitCount[v_position]>0):
            continue
        visitCount[v_position]+=1
        neighbours = problem.getSuccessors(v_position)
        for j in range(0, len(neighbours)):
            [neighbour_position, neighbour_direction, cost_to_neighbour] = neighbours[j]
            updated_directions = copy.copy(v_directions)
            updated_directions.append(neighbour_direction)
            total_cost = cost_to_neighbour + v_cost
            pq.update ( MyTriplet(neighbour_position, updated_directions, total_cost), total_cost)
    return []

def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """Search the node that has the lowest combined cost and heuristic first."""
    visitCount = util.Counter()
    pq = util.PriorityQueue()
    startState = problem.getStartState()
    visitCount[startState] = 1
    if(problem.isGoalState(startState)):
        return []
    start_state_successors = problem.getSuccessors(startState)
    # print start_state_successors
    for i in range(0, len(start_state_successors)):
        [state, direction, actual_cost] = start_state_successors[i]
        triplet = MyTriplet(state, [direction], actual_cost)
        predicted_cost = actual_cost + heuristic(state, problem) #+ heuristic(startState, problem)
        pq.push( triplet,  predicted_cost)
        # print "ss.........{0}".format(triplet)
    while(not(pq.isEmpty())):
        v = pq.pop();
        # print " v.........{0}".format(v)
        v_state = v.getState()
        v_directions = v.getDirections()
        v_actual_cost = v.getCost() # - heuristic(v_state, problem)
        if(problem.isGoalState(v_state)):
            return v_directions
        if(visitCount[v_state]>0):
            continue
        visitCount[v_state]+=1
        neighbours = problem.getSuccessors(v_state)
        for j in range(0, len(neighbours)):
            [neighbour_state, neighbour_direction, cost_to_neighbour] = neighbours[j]
            updated_directions = copy.copy(v_directions)
            updated_directions.append(neighbour_direction)
            actual_cost = v_actual_cost + cost_to_neighbour
            predicted_cost = actual_cost + heuristic(neighbour_state, problem)
            triplet = MyTriplet(neighbour_state, updated_directions, actual_cost)
            pq.update ( triplet, predicted_cost)
            # print " n.........{0}".format(triplet)
            # if(problem.isGoalState(neighbour_state)):
            #     return updated_directions
    return []


# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch
